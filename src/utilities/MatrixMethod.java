package utilities;

public class MatrixMethod {
	
	public static int[][] duplicate(int[][] input2dArray) {
		int rowSize = input2dArray.length;
		int columnSize = input2dArray[0].length;
		if(rowSize==columnSize) {
			throw new IllegalArgumentException("Array Cannot be Square");
		}
		int[][] newArray =  new int[rowSize][columnSize*2];
		
		
		int k = 0;
		for(int i = 0; i<newArray.length;i++) {
			for(int j = 0; j<newArray[i].length;j++,k++) {
				newArray[i][j] = input2dArray[i][k];
				
				if(k==2) {
					k = -1;
				}
			}
			
		}
		return newArray;	
	
	}
	
}

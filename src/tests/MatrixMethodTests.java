package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	void testDuplicate() {
		int[][] originalArray = {{1,2,3},{4,5,6}};
		int[][] expectedArray = {{1,2,3,1,2,3},{4,5,6,4,5,6}};
		int[][] newArray = MatrixMethod.duplicate(originalArray);
		
		assertArrayEquals(expectedArray,newArray);
		
	}

	@Test
	void testDuplicateException() {
		
		try{
			int[][] originalArray = {{1,2,3},{4,5,6},{7,8,9}};
			MatrixMethod.duplicate(originalArray);
		}
		catch(IllegalArgumentException e){
			System.out.println("Array cannot be Square");
		}
	}
}

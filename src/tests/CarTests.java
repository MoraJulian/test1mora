package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vehicles.Car;

class CarTests {

	@Test
	void testCarInitialization() {
		Car test = new Car(2);
		assertEquals(2,test.getSpeed());
	}
	
	@Test
	void testCarIntializationException() {
		try {
			Car test = new Car(-2);
		}
		catch(IllegalArgumentException e){
			System.out.print("Input illegal argument");
		}
		
	}
	
	@Test
	void testGetSpeedMethod() {
		Car test = new Car(5);
		assertEquals(5,test.getSpeed());
	}
	
	@Test
	void testGetLocationAfterMoveRight() {
		Car test = new Car(0);
		Car test2 = new Car(6);
		test.moveRight();
		test2.moveRight();
		assertEquals(0,test.getLocation());
		assertEquals(6,test2.getLocation());
	
	}

	@Test
	void testGetLocationAfterMoveleft() {
		Car test = new Car(3);
		test.moveLeft();
		assertEquals(-3,test.getLocation());
	}
	
	@Test
	void testAccelerate() {
		Car test = new Car(4);
		test.accelerate();
		assertEquals(5,test.getSpeed());
	}
	
	@Test
	void testDecelerate() {
		Car test = new Car(4);
		test.decelerate();
		assertEquals(3,test.getSpeed());
	}
	
	@Test
	void testDecelerateAtZeroSpeed() {
		Car test = new Car(0);
		test.decelerate();
		assertEquals(0,test.getSpeed());
	}
}
	